// Uses AMD or browser globals to create a module.

// Grabbed from https://github.com/umdjs/umd/blob/master/amdWeb.js.
// Check out https://github.com/umdjs/umd for more patterns.

// Defines a module "MobileWave".
// Note that the name of the module is implied by the file name. It is best
// if the file name and the exported global have matching names.

// If you do not want to support the browser global path, then you
// can remove the `root` use and the passing `this` as the first arg to
// the top function.

(function(root, factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    } else {
        // Browser globals
        root.MobileWave = factory();
    }
}(this, function() {
    'use strict';
    // Factory Function

    var mw = {};
    var _context = {};
    var _dGraph = {};
    var _iGraph = {};
    var _device = {};

    var _registrationURI = null;
    var _publicationURI = {};

    const TEMPLATE_AT_TS = "@ts";
    const TEMPLATE_AT_ID = "@id";
    const TEMPLATE_AT_TYPE = "@type";
    const TEMPLATE_AT_CONTEXT = "@context";
    const TEMPLATE_AT_GRAPH = "@graph";

    const TEMPLATE_MAPPING = "mapping";
    const TEMPLATE_TARGET = "target";
    const TEMPLATE_FIELDS = "fields";
    const TEMPLATE_PROPERTIES = "properties";

    const TEMPLATE_GENERATED = "generatedAt";
    /*************/
    /* Utilities */
    /*************/
    var request = function(method, url, parameter, doneCB, errorCB) {
        var xhr = new XMLHttpRequest();
        xhr.open(method, url, true);

        xhr.onload = function(e) {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    if (doneCB) {
                        doneCB(xhr.responseText);
                    }
                } else {
                    if (errorCB) {
                        errorCB();
                    }
                }
            }
        };
        xhr.send(parameter);
    }

    var weave = function(pattern, jObject, isProperty) {
        var mapping = Mustache.render(pattern[TEMPLATE_MAPPING], jObject);
        var target = pattern[TEMPLATE_TARGET];
        if (target) {
            var instanceId = Mustache.render(target, jObject);
            var obj = null;
            if (instanceId) {
                if (pattern["type"] === "dataProperty") {
                    obj = instanceId;
                } else {
                    obj = {};
                    obj[TEMPLATE_AT_ID] = instanceId;
                    if (!isProperty) {
                        obj[TEMPLATE_AT_TYPE] = mapping;
                    }
                }
                var properties = pattern[TEMPLATE_PROPERTIES];
                if (properties) {
                    properties.forEach(function(item, index) {
                        var prop = weave(item, jObject, true);
                        if (prop)
                            obj[item[TEMPLATE_MAPPING]] = prop;
                    });
                }
            }

            return obj;
        }
        throw "Missing target from the mapping!"
    };

    function MobileWave(options) {
        var init = function(template) {
            var jTemplate = (typeof template === "string" || template instanceof String) ? JSON.parse(template) : template;

            _context = jTemplate["context"];
            _dGraph = jTemplate["dGraph"];
            _iGraph = jTemplate["iGraph"];


            var dataSource = _device;
            dataSource[TEMPLATE_AT_TS] = Date.now();

            var graph = [];
            _dGraph.forEach(function(template, index) {
                graph.push(weave(template, dataSource));
            });
            graph[TEMPLATE_AT_ID] = "";

            if (_registrationURI) {
                var event = {};
                event[TEMPLATE_AT_CONTEXT] = _context;
                event[TEMPLATE_AT_GRAPH] = graph;
                // TODO: Revise the data to be sent
                // event[TEMPLATE_AT_TYPE] = "sao:StreamEvent";
                event[TEMPLATE_AT_ID] = Mustache.render("StreamEvent_{{@ts}}", dataSource)
                event[TEMPLATE_GENERATED] = new Date().toISOString();
                console.log("[mw]Registration URI:", _registrationURI);
                request("POST", _registrationURI, JSON.stringify(event), function(msg) {
                    console.log("[mw]Registration completed:", msg);
                });
            }
        };
        if (options["registrationURI"]) {
            _registrationURI = options["registrationURI"];
        } else {
            throw "Registration URI is not defined!";
        };
        if (options["publicationURI"]) {
            _publicationURI = options["publicationURI"];
        } else {
            throw "Publication URI is not defined!";
        };

        if (device || options["device"]) {
            _device = device;
        } else {
            throw "Device information is undefined!"
        }

        if (options["template"]) {
            init(options["template"]);
        } else if (options["templateURI"]) {
            request("GET", options["templateURI"], null, function(template) {
                init(templates);
            });
        } else {
            throw "No template source was provided, either define 'template' or 'templateURI'!"
        };
        return mw;
    };
    mw.transform = function(data, successCB) {
        console.log("[mw] transform");

        var dataSource = {};
        for (var key in _device) dataSource[key] = _device[key];
        for (var key in data) dataSource[key] = data[key];
        dataSource[TEMPLATE_AT_TS] = Date.now();

        var graph = [];
        _iGraph.forEach(function(template, index) {
            graph.push(weave(template, dataSource));
        });
        graph[TEMPLATE_AT_ID] = "";

        var event = {};
        event[TEMPLATE_AT_CONTEXT] = _context;
        event[TEMPLATE_AT_GRAPH] = graph;
        event[TEMPLATE_AT_ID] = Mustache.render("StreamEvent_{{@ts}}", dataSource)
        event[TEMPLATE_AT_TYPE] = "sao:StreamEvent";
        event[TEMPLATE_GENERATED] = new Date().toISOString();

        request("POST", _publicationURI, JSON.stringify(event), function(msg) {
            console.log("[mw]Publication completed:", msg);
        });


        if (successCB) {
            successCB(event);
        }
    };

    // Return a value to define the module export.
    // This example returns a functions, but the module
    // can return an object as the exported value.
    return MobileWave;
}));
